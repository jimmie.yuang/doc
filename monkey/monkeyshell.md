# <center> Monkey on Shell </center>

## 配置帮助

```shell
adb shell monkey -help
usage: monkey [-p ALLOWED_PACKAGE [-p ALLOWED_PACKAGE] ...]

              [-c MAIN_CATEGORY [-c MAIN_CATEGORY] ...]

              [--ignore-crashes] [--ignore-timeouts]

              [--ignore-security-exceptions]

              [--monitor-native-crashes] [--ignore-native-crashes]

              [--kill-process-after-error] [--hprof]

              [--pct-touch PERCENT] [--pct-motion PERCENT]

              [--pct-trackball PERCENT] [--pct-syskeys PERCENT]

              [--pct-nav PERCENT] [--pct-majornav PERCENT]

              [--pct-appswitch PERCENT] [--pct-flip PERCENT]

              [--pct-anyevent PERCENT] [--pct-pinchzoom PERCENT]

              [--pkg-blacklist-file PACKAGE_BLACKLIST_FILE]

              [--pkg-whitelist-file PACKAGE_WHITELIST_FILE]

              [--wait-dbg] [--dbg-no-events]

              [--setup scriptfile] [-f scriptfile [-f scriptfile] ...]

              [--port port]

              [-s SEED] [-v [-v] ...]

              [--sort-app-list]

              [--throttle MILLISEC] [--randomize-throttle]

              [--profile-wait MILLISEC]

              [--device-sleep-time MILLISEC]

              [--randomize-script]

              [--script-log]

              [--bugreport]

              [--periodic-bugreport]

              [--delay-appswitch MILLISEC]

              [--launch-app-after-launcher

              [--launch-app-after-app MILLISEC PACKAGE_NAME CLASS_NAME]

              COUNT
```

## 命令详解

### 常规

---

> `-v` 表示log等级,越多 `-v` 显示的log越详细

1. 没有 `-v` 选项,控制台剩基本没有log,只有结束的log
2. `-v` 提供时间百分比,以及少数事件信息
3. `-v -v` 提供更完整的事件信息,并且提供选中activity信息
4. `-v -v -v` 会提供未选中的activity信息

```shell
adb shell monkey -v 10
adb shell monkey -v -v 10
adb shell monkey -v -v -v 10
```

---

> `COUNT` 表示产生随机事件的次数

### 事件

---

> `-s <seed>` 作用：伪随机数生成器的seed值。
> 如果用相同的seed值再次运行monkey，将生成相同的事件序列。

```shell
adb shell monkey -s 12345 -v 10
```

---

> `--throttle <milliseconds>` 事件产生后,延迟x毫秒
> `--randomize-throttle` 每次延迟的时间随机

`--throttle` 选项不是每一次操作都会延迟(Sleeping for xx milliseconds),
而是几个事件,一次延迟,比较诡异

```shell
adb shell monkey --throttle 300 --randomize-throttle -v 100
```
表示每次延迟的时间随机 (0-300之间)

---
> `--pct-touch <percent>` 调整触摸时间的百分比

```
:Sending Touch (ACTION_DOWN): 0:(491.0,87.0)
:Sending Touch (ACTION_UP): 0:(485.08298,74.79217)
```

所谓的触摸事件是指在屏幕中的一个down-up事件，即在屏幕某处按下并抬起的操作
**即点击操作**

```shell
adb shell monkey –pct-touch 100 -v 10
```
---
> `--pct-motion <percent>` 调整motion事件百分比

```
:Sending Touch (ACTION_DOWN): 0:(245.0,829.0)
:Sending Touch (ACTION_MOVE): 0:(239.23427,833.75256)
:Sending Touch (ACTION_MOVE): 0:(228.66034,836.1514)
:Sending Touch (ACTION_MOVE): 0:(223.94264,837.66974)
:Sending Touch (ACTION_MOVE): 0:(210.5255,841.5436)
:Sending Touch (ACTION_MOVE): 0:(209.41461,848.32)
:Sending Touch (ACTION_MOVE): 0:(207.94354,853.94653
```

motion事件是由屏幕上某处一个down事件、一系列伪随机的移动事件和一个up事件组成
**即直线滑动事件**

```shell
adb shell monkey –pct-motion 100 -v 10
```
---

> `--pct-trackball <percent>` 调整滚动球事件百分比

```
:Sending Trackball (ACTION_MOVE): 0:(4.0,1.0)
:Sending Trackball (ACTION_MOVE): 0:(4.0,0.0)
:Sending Trackball (ACTION_MOVE): 0:(-1.0,-4.0)
:Sending Trackball (ACTION_MOVE): 0:(-2.0,2.0)
:Sending Trackball (ACTION_MOVE): 0:(3.0,-4.0)
:Sending Trackball (ACTION_MOVE): 0:(4.0,2.0)
:Sending Trackball (ACTION_MOVE): 0:(-3.0,4.0)
:Sending Trackball (ACTION_MOVE): 0:(-1.0,-5.0)
:Sending Trackball (ACTION_DOWN): 0:(0.0,0.0)
:Sending Trackball (ACTION_UP): 0:(0.0,0.0)
```

滚动球事件由一个或多个随机的移动事件组成，有时会伴随着点击事件
**移动包含曲线移动**

```shell
adb shell monkey –pct-trackball 100 -v 10
```
---

> `--pct-nav <percent>` 调整基本的导航事件百分比

```
:Sending Key (ACTION_DOWN): 22    // KEYCODE_DPAD_RIGHT
:Sending Key (ACTION_UP): 22    // KEYCODE_DPAD_RIGHT
```

导航事件由方向输入设备的上下左右按键所触发的事件组成
**目前智能手机不支持,都会转为 up/down事件**

---

> `--pct-majornav <percent>` 调整主要导航事件的百分比

```
:Sending Key (ACTION_DOWN): 82    // KEYCODE_MENU
:Sending Key (ACTION_UP): 82    // KEYCODE_MENU
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 23    // KEYCODE_DPAD_CENTER
:Sending Key (ACTION_UP): 23    // KEYCODE_DPAD_CENTER
```

这些导航事件通常会导致UI界面中的动作事件，如5-way键盘的中间键，回退按键、菜单按键
**目前智能手机不支持,都会转为 up/down事件**

---

> `--pct-syskeys <percent>`  调整系统事件百分比

```
:Sending Key (ACTION_DOWN): 4    // KEYCODE_BACK
:Sending Key (ACTION_UP): 4    // KEYCODE_BACK
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 24    // KEYCODE_VOLUME_UP
:Sending Key (ACTION_UP): 24    // KEYCODE_VOLUME_UP
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 3    // KEYCODE_HOME
:Sending Key (ACTION_UP): 3    // KEYCODE_HOME
```
这些按键通常由系统保留使用，如Home、Back、Start Call、End Call、音量调节

---

> `--pct-appswitch` 调整Activity启动的百分比

```
:Switch: #Intent;action=android.intent.action.MAIN;category=android.intent.category.LAUNCHER;launchFlags=0x10200000;component=cn.m4399.x2223.offerwalldemo/.MainActivity;end
    // Allowing start of Intent { act=android.intent.action.MAIN cat=[android.intent.category.LAUNCHER] cmp=cn.m4399.x2223.offerwalldemo/.MainActivity } in package cn.m4399.x2223.offerwalldemo
    // activityResuming(cn.m4399.x2223.offerwalldemo)
```

每次执行一次 `startActivity()` 方法.

---

> `--pct-pinchzoom` 缩放事件百分比

```
:Sending Touch (ACTION_DOWN): 0:(437.0,952.0)
:Sending Touch (ACTION_POINTER_DOWN 1): 0:(441.7197,960.0) 1:(533.0,957.0)
:Sending Touch (ACTION_MOVE): 0:(447.19562,960.0) 1:(538.3952,952.8984)
:Sending Touch (ACTION_MOVE): 0:(456.04355,960.0) 1:(540.0,937.5147)
:Sending Touch (ACTION_MOVE): 0:(464.12003,960.0) 1:(540.0,934.8646)
:Sending Touch (ACTION_MOVE): 0:(466.78018,960.0) 1:(540.0,931.7661)
:Sending Touch (ACTION_MOVE): 0:(470.09082,960.0) 1:(540.0,922.9404)
:Sending Touch (ACTION_MOVE): 0:(473.86096,960.0) 1:(540.0,914.4384)
:Sending Touch (ACTION_MOVE): 0:(476.2192,960.0) 1:(540.0,898.6724)
:Sending Touch (ACTION_MOVE): 0:(485.7193,960.0) 1:(540.0,895.0442)
:Sending Touch (ACTION_POINTER_UP 1): 0:(488.5932,960.0) 1:(540.0,887.84784)
:Sending Touch (ACTION_UP): 0:(492.3305,960.0)
```

---

> `--pct-rotation` 屏幕旋转事件百分比

```
:Sending rotation degree=3, persist=true
```

---

> `--pct-flip` 键盘翻转事件百分比

```
:Sending Flip keyboardOpen=false
```

---

> `--pct-anyevent` 其他事件百分比

```
:Sending Key (ACTION_DOWN): 163    // KEYCODE_NUMPAD_RIGHT_PAREN
:Sending Key (ACTION_UP): 163    // KEYCODE_NUMPAD_RIGHT_PAREN
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 71    // KEYCODE_LEFT_BRACKET
:Sending Key (ACTION_UP): 71    // KEYCODE_LEFT_BRACKET
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 212    // KEYCODE_EISU
:Sending Key (ACTION_UP): 212    // KEYCODE_EISU
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 233    // KEYCODE_TV_TELETEXT
:Sending Key (ACTION_UP): 233    // KEYCODE_TV_TELETEXT
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 108    // KEYCODE_BUTTON_START
:Sending Key (ACTION_UP): 108    // KEYCODE_BUTTON_START
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 130    // KEYCODE_MEDIA_RECORD
:Sending Key (ACTION_UP): 130    // KEYCODE_MEDIA_RECORD
Sleeping for 0 milliseconds
:Sending Key (ACTION_DOWN): 204    // KEYCODE_LANGUAGE_SWITCH
:Sending Key (ACTION_UP): 204    // KEYCODE_LANGUAGE_SWITCH
```
这包含所有其他事件，如按键、其他在设备上不常用的按钮等

### 限制条件

---
> `-p <allowed-package-name>` 待测的包名

```
adb shell monkey -p com.android.browser -v 10
```

多个 `-p` 可选择多个应用包

如果你指定一个或多个包，Monkey将只允许访问这些包中的Activity。

---

> `-c <main-category>`  指定activity

如果你指定一个或多个类别，Monkey将只允许系统启动这些指定类别中列出的Activity
一般有`Intent.CATEGORY_LAUNCHER`和`Intent.CATEGORY_MONKEY` 两种方式.


### 调试

---

> `--ignore-crashes` 运行中忽略crash，遇到crash依然把后面的事件跑完

---

> `--ignore-timeouts` 运行中忽略ANR,遇到ANR依然把后面的事件跑完

---

> `--ignore-security-exceptions` 忽略程序发生许可错误（例如启动一些需要许可的Activity）

---

> `--dbg-no-events` 设置此选项，Monkey将执行初始启动，进入一个测试Activity，并不会在进一步生成事件(待测试)

---
> `--hprof` 生成profilling报告。在data/misc路径下生成大文件（~5Mb）

---
> `--kill-process-after-error` 结束发生错的程序.并且停止monkey

---
> `--wait-dbg` 停止执行中的Monkey，直到有调试器和它相连接。

### 日志分析

```
// Event percentages:
//   0: 0.0%
//   1: 0.0%
//   2: 0.0%
//   3: 0.0%
//   4: -0.0%
//   5: 0.0%
//   6: 0.0%
//   7: 0.0%
//   8: 0.0%
//   9: 0.0%
//   10: 100.0%

设置事件百分比,所有的百分比加起来不能超过100%
    0：触摸事件百分比，即参数--pct-touch
    1：滑动事件百分比，即参数--pct-motion
    2：缩放事件百分比，即参数--pct-pinchzoom
    3：轨迹球事件百分比，即参数--pct-trackball
    4：屏幕旋转事件百分比，即参数--pct-rotation
    5：基本导航事件百分比，即参数--pct-nav
    6：主要导航事件百分比，即参数--pct-majornav
    7：系统事件百分比，即参数--pct-syskeys
    8：Activity启动事件百分比，即参数--pct-appswitch
    9：键盘翻转事件百分比，即参数--pct-flip
    10：其他事件百分比，即参数--pct-anyevent
```





























