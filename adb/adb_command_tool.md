# <center> adb 常用命令使用 </center>
---

1. 列出应用的Manifest.xml文件的内容

```shell
aapt dump xmltree <apk-file> AndroidManifest.xml
```

2. 列出在应用中注册的activity *(注册的activity不使用包含Activity来命名,则会导致不准确)*

```shell
aapt dump xmltree <apk-file> AndroidManifest.xml | grep name | grep Activity
```

3. 列出当前的activity

```bash
adb shell dumpsys activity top | grep ACTIVITY
或者
adb shell dumpsys activity activities | grep mFocusedActivity
```

4. 清除应用的数据和缓存

```bash
adb shell pm clear <packagename>
```

5. 列出手机装的app的包名

```bash
# 全部应用
adb shell pm list packages

# 系统应用
adb shell pm list packages -s

# 第三方应用
adb shell pm list packages -3
```















