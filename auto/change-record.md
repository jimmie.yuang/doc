# <center> 变更记录 </center>

## 2017.11.9

1. 对未适配机型做默认安装处理.

## 2017.11.7

1.  添加遍历时间信息(运行时间)
2. 对设备断开的异常进行处理,做中断处理.

## 2017.11.2

1. android 4.4 及以上,webView登录适配. 添加 `index` 字段来判断.
2. 添加时间控制,只对主体遍历有效
3. 对登录后是否成功,进行在判断.截图以及log报告
4. 添加是否强制重装被测应用(`installable`字段)


## 2017.5.18(2)
1. 解决空指针异常

```java
java.lang.NullPointerException
        at cn.m4399.autotraveller.algorithm.handler.BackHandler.tryRollBack(BackHandler.java:26)
        at cn.m4399.autotraveller.algorithm.handler.HandlerFacade.tryRollBack(HandlerFacade.java:34)
        at cn.m4399.autotraveller.algorithm.executor.SearchExecutor.handleUI(SearchExecutor.java:168)
        at cn.m4399.autotraveller.algorithm.executor.SearchExecutor.search(SearchExecutor.java:91)
        at cn.m4399.autotraveller.algorithm.executor.SearchExecutor.search(SearchExecutor.java:110)
        at cn.m4399.autotraveller.algorithm.executor.SearchExecutor.search(SearchExecutor.java:110)
        at cn.m4399.autotraveller.algorithm.executor.SearchExecutor.search(SearchExecutor.java:110)
        at cn.m4399.autotraveller.algorithm.Executor.search(Executor.java:61)
        at cn.m4399.autotraveller.core.driver.IDriver.startEngine(IDriver.java:152)
        at cn.m4399.autotraveller.core.driver.AutoDriver.run(AutoDriver.java:30)
        at cn.m4399.autotraveller.core.driver.AutoDriver.travel(AutoDriver.java:47)
        at cn.m4399.autotraveller.Traveller.lambda$startDriver$0(Traveller.java:136)
        at cn.m4399.autotraveller.Traveller$$Lambda$2/591137559.run(Unknown Source)
```

## 2017.5.18

1. 遍历添加层级深度控制 `depth`
2. `action` 类型添加 `back`类型,表示按返回键
3. 添加可配置的卸载选项 `uninstalls`
4. 修复滑动列表遍历过程中,可操作元素的获取逻辑,解决不可滑动列表,最后一项元素无法遍历到的问题
5. 添加账号库管理 `accounts`
6. `input` 类型操作,添加 `inputType`字段,值 `usrName`表示输入账号, 值 `password`表示输入密码

## 2017.5.11

1. 去掉 `waitingGap` 字段
2. 新添全局变量 `caseGap` 表示用例之间的间隔时间
3. `before/after` 新添 `actionGap` 表示用例步骤之间的 时间间隔
4. `before/after` 新添 `caseName` 表示用例的名字

## 2017.5.10

1. 添加`waitingGap`字段,表示用例的间隔时间

## 2017.5.4

1. 添加字段 `searchable` ,用来表示,是否进行主体的遍历 <br />
     true : 遍历<br />
     false : 不遍历
2. `before/after` 中 `needFinsh`字段 改为 `needSearch` <br />

## 2017.5.3

1. 在配置操作项是,支持 `loopTimes`,`timeSpacing`,`enableAlter`,`errorMessage` 配置 <br />
	`loopTimes` : 循环次数 ,表示每次检查配置控件出现的次数 <br />
	`timeSpacing` : 每次检查控件出现的间隔时间 数值(毫秒单位)<br />
	(loopTime * timeSpacing 为等待控件的超时时间)<br />
	`enableAlter` : 是否自己处理对话框 (因为对话框的行为是alter关键字配置的,<br />
	所以如果路径链配置中出现对话框,也会被当做系统对话框处理,所以需要单独的配置告诉程序,由路径链来处理对话框)<br />
	`errorMessage` : 配置错误信息 (用于配置路径找不到,或者出错的提示,易于定位问题)

2. 遍历程序支持 `before` ,`after` 配置<br />
	before 表示在遍历开始之前执行的配置操作 (该配置在登录之后执行)<br />
	after 表示在遍历结束后执行的配置操作 (该配置在报告写入完成之前执行)

3. 遍历程序支持选择设备 命令行添加 -u <设备名>  <br />
	用于本地程序,可选择设备进行操作, 设备名为adb devices 的值<br />
	支持多设备选择 -u xxx -u xxx<br />
	如果没有-u选项 则默认执行连接电脑的所有设备

4. 遍历程序支持在指定时间内结束 (修复之前程序配置时间,在多设备中会出异常的bug)<br />
	`duration` : 数值(秒为单位) 最好配置 大于 10分钟

5.  `before/after` 支持 `chain` , `reEnter` , `needFinsh` 关键字
